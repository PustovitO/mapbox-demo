package ua.com.ef_mapbox.mvp

/**
 * Create by Pustovit Oleksandr on 26.03.2019
 */

interface MVPPresenter<View : MVPView> {
    fun onAttachView(view: View)
    fun onDetachView()
}