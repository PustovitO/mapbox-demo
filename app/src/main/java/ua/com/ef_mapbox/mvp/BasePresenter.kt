package ua.com.ef_mapbox.mvp

import io.reactivex.disposables.CompositeDisposable

/**
 * Create by Pustovit Oleksandr on 26.03.2019
 */
abstract class BasePresenter<View : MVPView> : MVPPresenter<View> {

    protected var mvpView: View? = null

    protected var disposables = CompositeDisposable()

    val isViewAttached: Boolean
        get() = mvpView == null

    override fun onAttachView(view: View) {
        mvpView = view
    }

    override fun onDetachView() {
        mvpView = null
        disposables.clear()
    }
}