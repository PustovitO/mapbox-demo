package ua.com.ef_mapbox.test.presenter

import ua.com.ef_mapbox.mvp.MVPPresenter
import ua.com.ef_mapbox.test.GeometryView

/**
 * Create by Pustovit Oleksandr on 26.03.2019
 */
interface GeometryPresenter : MVPPresenter<GeometryView> {
    fun loadGeometry()
    fun loadTrack()
}