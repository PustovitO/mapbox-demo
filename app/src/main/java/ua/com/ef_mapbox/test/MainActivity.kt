package ua.com.ef_mapbox.test

import android.Manifest
import android.animation.ObjectAnimator
import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.material.snackbar.Snackbar
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Feature.fromGeometry
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.FillLayer
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import ua.com.ef_mapbox.R
import ua.com.ef_mapbox.test.presenter.GeometryPresenterImpl
import java.io.BufferedReader
import java.io.InputStreamReader


class MainActivity : AppCompatActivity(), OnMapReadyCallback, PermissionsListener, GeometryView,
    ValueAnimator.AnimatorUpdateListener {

    private val REQUEST_CODE_ASK_PERMISSIONS = 123
    private val LAYER_FIELD = "layer-field"
    private val LINE_FIELD = "line-id"
    private val SOURCE_FIELD = "source-field"
    private val SOURCE_TRACK = "source-track"
    private val LINE_TRACK = "line-track"

    private val TRACK_MARKER = "track-marker"
    private val TRACK_MARKER_LAYER = "track-marker-layer"
    private val TRACK_MARKER_ID = "track-marker-id"

    private var mapboxMap: MapboxMap? = null
    private var mapStyle: Style? = null
    private lateinit var presenter: GeometryPresenterImpl
    private var animator: ValueAnimator? = null
    private var currentPosition: LatLng? = null
    private val typeEvaluator = MarkerTypeEvaluator()


    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {

    }

    override fun onPermissionResult(granted: Boolean) {
        if (!granted) {
            val permissionList = ArrayList<String>()
            permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION)
            ActivityCompat.requestPermissions(this, permissionList.toTypedArray(), REQUEST_CODE_ASK_PERMISSIONS)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        presenter = GeometryPresenterImpl(this)
        presenter.onAttachView(this)
        initMap(savedInstanceState)
    }

    fun initMap(savedInstanceState: Bundle?) {
        map.onCreate(savedInstanceState)
        map.getMapAsync(this)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        mapboxMap.setStyle(Style.SATELLITE) { style ->
            mapStyle = style
            enableLocationComponent(style)
            style.addImage(TRACK_MARKER_ID, BitmapFactory.decodeResource(resources, R.drawable.ic_action_name))
            presenter.loadTrack()
        }
        mapboxMap.locationComponent
    }

    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            mapboxMap?.let {
                val locationComponent = it.locationComponent
                // Activate with options
                locationComponent.activateLocationComponent(this, loadedMapStyle)
                // Enable to make component visible
                locationComponent.isLocationComponentEnabled = true
                // Set the component's camera mode
                locationComponent.cameraMode = CameraMode.TRACKING
                // Set the component's render mode
                locationComponent.renderMode = RenderMode.COMPASS
                locationComponent.lastKnownLocation?.let { location ->
                    val position = CameraPosition.Builder()
                        .target(LatLng(location.latitude, location.longitude)) // Sets the new camera position
                        .zoom(17.0) // Sets the zoom
                        .build() // Creates a CameraPosition from the builder
                    mapboxMap?.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000)
                }
            }
            // Get an instance of the component

        } else {
            val permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    private fun loadGeoJSON(): String {
        val inputStream = resources.openRawResource(R.raw.fields)

        val inputreader = InputStreamReader(inputStream)
        val buffreader = BufferedReader(inputreader)
        var line: String? = buffreader.readLine()
        val text = StringBuilder()

        try {
            while (line != null) {
                text.append(line)
                line = buffreader.readLine()
            }
        } catch (e: Exception) {
            return ""
        }

        return text.toString()
    }

    override fun postGeoJson(json: String) {
        mapStyle?.let {

            var source = it.getSource(SOURCE_FIELD)
            if (source == null) {
                source = GeoJsonSource(SOURCE_FIELD, json)
                it.addSource(GeoJsonSource(SOURCE_FIELD, json))
                it.addLayer(
                    FillLayer(LAYER_FIELD, SOURCE_FIELD).withProperties(
                        fillColor(Color.parseColor("#681e88e5")), fillOutlineColor(Color.RED)
                    )
                )
                it.addLayer(
                    LineLayer(LINE_FIELD, SOURCE_FIELD).withProperties(
                        lineColor(Color.RED), lineWidth(2f)
                    )
                )
            } else
                (source as GeoJsonSource).apply {
                    this.setGeoJson(json)
                    it.getLayer(LAYER_FIELD)
                        ?.setProperties(fillColor(Color.parseColor("#681e88e5")), fillOutlineColor(Color.RED))
                    it.getLayer(LINE_FIELD)?.setProperties(lineColor(Color.RED), lineWidth(2f))
                }
        }
    }

    override fun postGeoJsonTrack(json: String) {
        mapStyle?.let {
            it.addSource(GeoJsonSource(SOURCE_TRACK, json))
            /*it.addLayer(
            FillLayer(LAYER_FIELD, SOURCE_FIELD).withProperties(
                fillColor(Color.parseColor("#681e88e5")), fillOutlineColor(Color.RED)
            )
        )*/
            it.addLayer(
                LineLayer(LINE_TRACK, SOURCE_TRACK).withProperties(
                    lineColor(Color.RED), lineWidth(2f)
                )
            )
        }
    }

    override fun postLineStringTrack(lineString: LineString, point: Point) {
        mapStyle?.let {
            var source = it.getSource(SOURCE_TRACK)
            if (source == null) {
                it.addSource(GeoJsonSource(SOURCE_TRACK, Feature.fromGeometry(lineString)))
                it.addLayer(
                    LineLayer(LINE_TRACK, SOURCE_TRACK).withProperties(
                        lineColor(Color.RED), lineWidth(2f)
                    )
                )
            } else {
                (source as GeoJsonSource).apply {
                    this.setGeoJson(fromGeometry(lineString))
                }
            }
            val position = CameraPosition.Builder()
                .target(LatLng(point.latitude(), point.longitude()))
                .zoom(18.0)
                .build()
            mapboxMap?.animateCamera(CameraUpdateFactory
                .newCameraPosition(position),100)
            addMarker(point, it)
        }
    }

    private fun addMarker(point: Point, style: Style) {
        // Add the marker image to map
        if (style.getSource(TRACK_MARKER) == null) {
            currentPosition = LatLng(point.latitude(), point.longitude())
            val geoJsonSource = GeoJsonSource(TRACK_MARKER, Feature.fromGeometry(point))
            style.addSource(geoJsonSource)
            val symbolLayer = SymbolLayer(TRACK_MARKER_LAYER, TRACK_MARKER)
            symbolLayer.withProperties(
                PropertyFactory.iconImage(TRACK_MARKER_ID),
                PropertyFactory.iconIgnorePlacement(true),
                PropertyFactory.iconAllowOverlap(true)
            )
            style.addLayer(symbolLayer)
        } else {
            animateMarker(point)
            //(style.getSource(TRACK_MARKER) as GeoJsonSource).setGeoJson(Feature.fromGeometry(point))
        }
    }

    private fun animateMarker(point: Point) {
        if (animator != null && animator?.isStarted!!) {
            currentPosition = animator?.animatedValue as LatLng?
            animator?.cancel()
        }

        animator = ObjectAnimator.ofObject(typeEvaluator, currentPosition, LatLng(point.latitude(), point.longitude()))
            .setDuration(100)
        animator?.addUpdateListener(this)
        animator?.start()

        currentPosition = LatLng(point.latitude(), point.longitude())
    }

    override fun onAnimationUpdate(valueAnimator: ValueAnimator) {
        val animatedPosition = valueAnimator.animatedValue as LatLng
        mapStyle?.let {
            (it.getSource(TRACK_MARKER) as GeoJsonSource).setGeoJson(
                Point.fromLngLat(
                    animatedPosition.longitude,
                    animatedPosition.latitude
                )
            )
        }

    }

// Class is used to interpolate the marker animation.
/*    private val TypeEvaluator<LatLng> latLngEvaluator = TypeEvaluator<LatLng>()
    {

        private final LatLng latLng = new LatLng();

        @Override
        public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
            latLng.setLatitude(
                startValue.getLatitude()
                        + ((endValue.getLatitude() - startValue.getLatitude()) * fraction)
            );
            latLng.setLongitude(
                startValue.getLongitude()
                        + ((endValue.getLongitude() - startValue.getLongitude()) * fraction)
            );
            return latLng;
        }
    };*/

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStop() {
        super.onStop()
        map.onStop()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.let {
            map.onSaveInstanceState(it)
        }
    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    public override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onStart() {
        super.onStart()
        map.onStart()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        map.onDestroy()
    }
}
