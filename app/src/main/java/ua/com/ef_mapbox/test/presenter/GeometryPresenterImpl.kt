package ua.com.ef_mapbox.test.presenter

import android.content.Context
import android.util.Log
import androidx.annotation.RawRes
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONObject
import ua.com.ef_mapbox.R
import ua.com.ef_mapbox.data.FieldEntity
import ua.com.ef_mapbox.data.TrackEntity
import ua.com.ef_mapbox.mvp.BasePresenter
import ua.com.ef_mapbox.test.GeometryView
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.concurrent.TimeUnit

/**
 * Create by Pustovit Oleksandr on 26.03.2019
 */
class GeometryPresenterImpl(val context: Context) : BasePresenter<GeometryView>(), GeometryPresenter {
    private var jsonRes : String = "{" +
            "\"type\": \"FeatureCollection\"," +
            "\"features\": [" +
            "]}"

    private val header =  "{\"type\": \"Feature\", " +
            "      \"properties\": {}," +
            "      \"geometry\": "
    override fun loadGeometry() {
        disposables.add(
            Observable.fromCallable<String> { loadGeoJSON(R.raw.fields) }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    createGeometryList(it)
                }, {
                    Log.d("R.raw.fields", it.localizedMessage)
                })
        )
    }

    override fun loadTrack() {
        disposables.add(
            Observable.fromCallable<String> { loadGeoJSON(R.raw.mira_track) }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    createTrack(it)
                }, {
                    Log.d("R.raw.fields", it.localizedMessage)
                })
        )
    }

    private fun createTrack(data: String) {
        disposables.addAll(Observable.fromCallable { TrackEntity(JSONObject(data).getJSONArray("features")) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                emitTrackGeometry(it)
            },{
                Log.d("createTrack", it.localizedMessage)
            }))
        /*val track =
        mvpView?.postLineStringTrack(track.lineString)*/
    }

    private fun emitTrackGeometry(trackEntity: TrackEntity){
        val latLonList: MutableList<Point> = ArrayList()
        disposables.add(Observable
            .interval(0, 100, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .map { i -> trackEntity.latLonList[i.toInt()] }
            .take(trackEntity.latLonList.size.toLong())
            .subscribe {
                latLonList.add(it)
                mvpView?.postLineStringTrack(LineString.fromLngLats(latLonList), it)
            })
    }


    private fun createGeometryList(json: String) {
        disposables.add(Observable.fromCallable<ArrayList<FieldEntity>> { fillFieldsList(json) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                emitGeometry(it)
            }, {
                Log.d("R.raw.fields", it.localizedMessage)
            }))
    }

    private fun fillFieldsList(json: String): ArrayList<FieldEntity> {
        val list = ArrayList<FieldEntity>()
        val array = JSONArray(json).getJSONArray(0)
        for (i in 0 until array.length()) {
            list.add(FieldEntity(array.getJSONObject(i)))
        }
        return list
    }

    private fun emitGeometry(list: List<FieldEntity>) {
        var counter = 0
        disposables.add(Observable
            .interval(0, 200, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .map { i -> list[i.toInt()] }
            .take(list.size.toLong())
            .subscribe {
                val last = if(counter == 0) "" else ","
                jsonRes = jsonRes.substring(0, jsonRes.length - 2)+ last + header + it.geoJson + "}]}"
                Log.d("emitGeometry", it.name)
                mvpView?.postGeoJson(jsonRes)
                counter++
            })
    }

    private fun loadGeoJSON(@RawRes fileId: Int): String {
        Log.d("loadGeoJSON", "START")
        val inputStream = context.resources.openRawResource(fileId)

        val inputreader = InputStreamReader(inputStream)
        val buffreader = BufferedReader(inputreader)
        var line: String? = buffreader.readLine()
        val text = StringBuilder()

        try {
            while (line != null) {
                text.append(line)
                line = buffreader.readLine()
            }
        } catch (e: Exception) {
            return ""
        }
        Log.d("loadGeoJSON", "END")
        return text.toString()
    }
}