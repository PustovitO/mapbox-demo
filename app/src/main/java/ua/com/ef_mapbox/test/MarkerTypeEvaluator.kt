package ua.com.ef_mapbox.test

import android.animation.TypeEvaluator
import com.mapbox.mapboxsdk.geometry.LatLng

/**
 * Create by Pustovit Oleksandr on 28.03.2019
 */
class MarkerTypeEvaluator : TypeEvaluator<LatLng> {
    private val latLng = LatLng()

    override fun evaluate(fraction: Float, startValue: LatLng, endValue: LatLng): LatLng {
        latLng.latitude = (startValue.latitude + ((endValue.latitude - startValue.latitude) * fraction))
        latLng.longitude = (startValue.longitude + ((endValue.longitude - startValue.longitude) * fraction))
        return latLng
    }
}