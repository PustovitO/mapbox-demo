package ua.com.ef_mapbox.test

import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import ua.com.ef_mapbox.mvp.MVPView

/**
 * Create by Pustovit Oleksandr on 26.03.2019
 */
interface GeometryView : MVPView {
    fun postGeoJson(json: String)
    fun postGeoJsonTrack(json: String)
    fun postLineStringTrack(lineString: LineString, point: Point)

}