package ua.com.ef_mapbox

import android.app.Application
import com.mapbox.mapboxsdk.Mapbox

/**
 * Create by Pustovit Oleksandr on 22.03.2019
 */
class EfMapBoxApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Mapbox.getInstance(this, getString(R.string.mapbox_token))
    }
}