package ua.com.ef_mapbox.data

import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import org.json.JSONArray
import org.json.JSONObject

/**
 * Create by Pustovit Oleksandr on 27.03.2019
 */
class TrackEntity(json: JSONArray) {
    val latLonList: MutableList<Point>
    //val lineString : LineString

    init {
       // val features = json.getJSONArray("features")
        latLonList = ArrayList()
        for (i in 0 until json.length()) {
            val object_ = JSONObject(json[i].toString())
            val coordinates = object_.getJSONObject("geometry").getJSONArray("coordinates")
            for (j in 0 until coordinates.length()) {
                val latLng = JSONArray(coordinates[j].toString())
                latLonList.add(Point.fromLngLat(latLng[0] as Double, latLng[1] as Double))
            }
        }
        //lineString = LineString.fromLngLats(latLonList)
    }
}