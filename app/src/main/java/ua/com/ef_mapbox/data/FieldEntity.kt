package ua.com.ef_mapbox.data

import com.mapbox.geojson.*
import com.mapbox.mapboxsdk.geometry.LatLng
import org.json.JSONArray
import org.json.JSONObject

/**
 * Create by Pustovit Oleksandr on 26.03.2019
 */
class FieldEntity(json: JSONObject) {
    val name: String
    val geoJson: String
    val uid: String
   /* val latLonList: MutableList<Point>
    val lineString : LineString*/

    init {
        name = json.getString("name")
        uid = json.getString("uid")
        val geometry = json.getJSONObject("geom")
        //val coordinates: JSONArray = geometry.getJSONArray("coordinates")[0] as JSONArray
        /*latLonList = ArrayList()
        for (i in 0 until coordinates.length()) {
            val latLng = JSONArray(coordinates[i].toString())
            latLonList.add(Point.fromLngLat(latLng[0] as Double, latLng[1] as Double))
        }
        lineString = LineString.fromLngLats(latLonList)*/
        geoJson = geometry.toString()
    }
}